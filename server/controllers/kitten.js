import Kitten from '../models/kitten';
import cuid from 'cuid';

export default function () {
  Kitten.count().exec((err, count) => {
    // Check if existed
    if (count > 0) {
      return;
    }
    // Schema population
    const kitten1 = new Kitten({ name: 'Sparkle', cuid: cuid() });
    const kitten2 = new Kitten({ name: 'Spot', cuid: cuid() });
    const kitten3 = new Kitten({ name: 'Flip', cuid: cuid() });
    const kitten4 = new Kitten({ name: 'Sarch', cuid: cuid() });
    const kitten5 = new Kitten({ name: 'Comby', cuid: cuid() });
    // Populate Table
    Kitten.create([kitten1, kitten2, kitten3, kitten4, kitten5], (error) => {
      if (!error) {
        console.log('ready to go....');
      }
    });
  });
}
