import React from 'react';
import {
  Link,
} from 'react-router-dom';
import { connect } from 'react-redux'

// import styles from './mern.css';

const Tickets = () => {
  return (
    <div className="app">
      <div className="welcome">
        <Link className="link" to="/">Home</Link>
        <Link className="link" to="/mern">Mern</Link>
        <Link className="link" to="/contact">Contact</Link>
        <h1>MERN</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora obcaecati, impedit iste! Quia necessitatibus quidem atque, molestiae recusandae voluptatibus nostrum nihil illum ullam nesciunt alias eligendi eius neque ipsum provident!</p>
        <img src={require('../images/node.png')} className="image" />
      </div>
    </div>
  )
}


export default Tickets;
// 'use strict';

// const Tickets = require('./Models/tickets');

// exports.createTicket = function(req, res, next) {  
//   const name = req.body.name;
//   const email = req.body.email;
//   const message = req.body.message;

//   if (!name) {
//     return res.status(422).send({ error: 'You must enter a name!'});
//   }

//   if (!email) {
//     return res.status(422).send({ error: 'You must enter your email!'});
//   }

//   if (!message) {
//     return res.status(422).send({ error: 'You must enter a detailed description of what you need!'});
//   }

//   let ticket = new Tickets({
//     name: name,
//     email: email,
//     status: "Open",
//     message: message
//   });

//   ticket.save(function(err, user) {
//     if(err) {return next(err);}

//     res.status(201).json({ message: "Thanks! Your request was submitted successfuly!" });
//     next();
//   })
// }
