const config = {
  mongoURL: process.env.MONGO_URL || 'mongodb://dxroot:dentsux12345@127.0.0.1:27017/mernstack?authSource=admin',
  port: process.env.PORT || 8122,
};

export default config;
