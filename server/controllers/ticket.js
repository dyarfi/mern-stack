import Ticket from '../models/ticket';
import cuid from 'cuid';

export default function () {
  Ticket.count().exec((err, count) => {
    // Check if existed
    if (count > 0) {
      return;
    }
    // Schema population
    const ticket1 = new Ticket({ name: 'Brad Traversy', email: 'brad@traversy.com', status: '1', message:'This is ticket one' });
    const ticket2 = new Ticket({ name: 'Jimmy Fallon', email: 'jimmyfallon@gmail.com', status: '1', message:'This is ticket two' });
    const ticket3 = new Ticket({ name: 'John Doe', email: 'johndoe@gmail.com', status: '1', message:'This is ticket three' });
    const ticket4 = new Ticket({ name: 'Jane Swatson', email: 'janeswatson@gmail.com', status: '1', message:'This is ticket four' });
    const ticket5 = new Ticket({ name: 'Dr. Jekyll', email: 'drjekyll@gmail.com', status: '1', message:'This is ticket five' });
    // Populate Table
    Ticket.create([ticket1, ticket2, ticket3, ticket4, ticket5], (error) => {
      if (!error) {
        console.log('ready to go....');
      }
    });
  });
}
